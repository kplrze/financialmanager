﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialManager.Model
{
    public class Income
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public decimal Amount { get; set; }
    }
}
