﻿using GalaSoft.MvvmLight;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialManager.Model
{
    [Table("Categories")]
    public class Category : ObservableObject
    {
        #region Id
        [PrimaryKey, Unique, AutoIncrement]
        public int Id { get; set; } 
        #endregion

        #region Name
        private string name = String.Empty;
        public string Name
        {
            get { return name; }
            set
            {
                if (name == value)
                {
                    return;
                }
                name = value;
                RaisePropertyChanged("Name");
            }
        }
        #endregion
    }
}
