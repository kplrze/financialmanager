﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Windows.UI.Xaml.Media.Imaging;

namespace FinancialManager.Model
{
    public class MainMenuItem
    {
        public string Text { get; set; }
        public Uri Icon { get; set; }
        public bool Visible { get; set; }
        public Type Page { get; set; }
        public bool MasterPage { get; set; }
    }
}
