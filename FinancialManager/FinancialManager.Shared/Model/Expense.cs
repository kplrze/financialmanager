﻿using GalaSoft.MvvmLight;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialManager.Model
{
    [Table("Expense")]
    public class Expense : ObservableObject
    {
        [PrimaryKey, Unique, AutoIncrement]
        public int Id { get; set; }

        #region DateCreated()
        private DateTime dateCreated = DateTime.Now;
        public DateTime DateCreated
        {
            get { return dateCreated; }
            set
            {
                if (dateCreated == value)
                {
                    return;
                }
                dateCreated = value;
                RaisePropertyChanged("DateCreated");
            }
        } 
        #endregion

        #region Name
        private string name = String.Empty;
        public string Name
        {
            get { return name; }
            set
            {
                if (name == value)
                {
                    return;
                }
                name = value;
                RaisePropertyChanged("Name");
            }
        } 
        #endregion

        #region CategoryId
        private int categoryId = 0;
        public int CategoryId
        {
            get { return categoryId; }
            set
            {
                if (categoryId == value)
                {
                    return;
                }
                categoryId = value;
                RaisePropertyChanged("CategoryId");
            }
        } 
        #endregion

        #region Amount
        private decimal amount = 0;
        public decimal Amount
        {
            get { return amount; }
            set
            {
                if (amount == value)
                {
                    return;
                }
                amount = value;
                RaisePropertyChanged("Amount");
            }
        }
        #endregion

        #region Details
        private string details = String.Empty;
        public string Details
        {
            get { return details; }
            set
            {
                if (details == value)
                {
                    return;
                }
                details = value;
                RaisePropertyChanged("Details");
            }
        } 
        #endregion
    }
}
