﻿using FinancialManager.Categories;
using FinancialManager.Common;
using FinancialManager.Expenses;
using FinancialManager.Incomes;
using FinancialManager.Model;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Windows.UI.Xaml.Media.Imaging;

namespace FinancialManager.ViewModel
{
    public class MasterPageViewModel
    {
        private ObservableCollection<MainMenuItem> mainMenuItems = null;
        public ObservableCollection<MainMenuItem> MainMenuItems
        {
            get { return mainMenuItems; }
            private set { mainMenuItems = value; }
        }

        public ICommand MainMenuSelectionChangedCmd { get; private set; }
        public ICommand GoToPageCmd { get; private set; }
        public ICommand GoToExpenseEditPageCmd { get; private set; }
        public ICommand GoToIncomeEditPageCmd { get; private set; }
        public ICommand GoToCategoryEditPageCmd { get; set; }

        public MasterPageViewModel()
        {
            mainMenuItems = new ObservableCollection<MainMenuItem>();

            this.GoToPageCmd = new RelayCommand<Type>(GoToPage);
            this.GoToExpenseEditPageCmd = new RelayCommand(GoToExpenseEditPage);
            this.GoToIncomeEditPageCmd = new RelayCommand(GoToIncomeEditPage);
            this.GoToCategoryEditPageCmd = new RelayCommand(GoToCategoryEditPage);
        }

        #region Commands methods
        private void GoToPage(Type page)
        {
            NavigateService.Instance.Navigate(page, typeof(MasterPage));
        }

        private void GoToExpenseEditPage()
        {
            NavigateService.Instance.Navigate(typeof(ExpenseEditPage));
        }

        private void GoToIncomeEditPage()
        {
            NavigateService.Instance.Navigate(typeof(IncomeEditPage));
        }

        private void GoToCategoryEditPage()
        {
            NavigateService.Instance.Navigate(typeof(CategoryEditPage));
        }
        #endregion

        public void ReloadMenus()
        {
            try
            {
                var loader = new Windows.ApplicationModel.Resources.ResourceLoader();

                mainMenuItems.Clear();

                MainMenuItem balanceMenuItem = new MainMenuItem();
                balanceMenuItem.Text = loader.GetString("MainMenu_Balance");
                balanceMenuItem.Icon = new Uri("ms-appx:///Themes/Light/Balance3.png", UriKind.Absolute);
                balanceMenuItem.Visible = true;
                balanceMenuItem.Page = typeof(ExpensesMainPage);
                balanceMenuItem.MasterPage = true;

                mainMenuItems.Add(balanceMenuItem);

                MainMenuItem expenseMenuItem = new MainMenuItem();
                expenseMenuItem.Text = loader.GetString("MainMenu_Expenses");
                expenseMenuItem.Icon = new Uri("ms-appx:///Themes/Light/Expenses2.png", UriKind.Absolute);
                expenseMenuItem.Visible = true;
                expenseMenuItem.Page = typeof(ExpensesMainPage);
                expenseMenuItem.MasterPage = true;

                mainMenuItems.Add(expenseMenuItem);

                MainMenuItem incomesMenuItem = new MainMenuItem();
                incomesMenuItem.Text = loader.GetString("MainMenu_Incomes");
                incomesMenuItem.Icon = new Uri("ms-appx:///Themes/Light/Incomes2.png", UriKind.Absolute);
                incomesMenuItem.Page = typeof(IncomesMain);
                incomesMenuItem.MasterPage = true;

                mainMenuItems.Add(incomesMenuItem);

                MainMenuItem reportsMenuItem = new MainMenuItem();
                reportsMenuItem.Text = loader.GetString("MainMenu_Statements_Charts");
                reportsMenuItem.Icon = new Uri("ms-appx:///Themes/Light/Reports2.png", UriKind.Absolute);
                reportsMenuItem.Visible = true;
                reportsMenuItem.Page = typeof(ExpensesMainPage);
                reportsMenuItem.MasterPage = true;

                mainMenuItems.Add(reportsMenuItem);

                MainMenuItem templatesMenuItem = new MainMenuItem();
                templatesMenuItem.Text = loader.GetString("MainMenu_Templates");
                templatesMenuItem.Icon = new Uri("ms-appx:///Themes/Light/Templates2.png", UriKind.Absolute);
                templatesMenuItem.Visible = true;
                templatesMenuItem.Page = typeof(ExpensesMainPage);
                templatesMenuItem.MasterPage = true;

                mainMenuItems.Add(templatesMenuItem);

                MainMenuItem dictionariesMenuItem = new MainMenuItem();
                dictionariesMenuItem.Text = loader.GetString("MainMenu_Dictionaries");
                dictionariesMenuItem.Icon = new Uri("ms-appx:///Themes/Light/Dictionaries2.png", UriKind.Absolute);
                dictionariesMenuItem.Visible = true;
                dictionariesMenuItem.Page = typeof(ExpensesMainPage);
                dictionariesMenuItem.MasterPage = true;

                mainMenuItems.Add(dictionariesMenuItem);

                MainMenuItem categoriesMenuItem = new MainMenuItem();
                categoriesMenuItem.Text = loader.GetString("MainMenu_Categories");
                categoriesMenuItem.Icon = new Uri("ms-appx:///Themes/Light/Categories.png", UriKind.Absolute);
                categoriesMenuItem.Visible = true;
                categoriesMenuItem.Page = typeof(CategoriesMainPage);
                categoriesMenuItem.MasterPage = true;

                mainMenuItems.Add(categoriesMenuItem);
            }
            catch(Exception ex)
            {
                //TODO
            }
        }
    }
}
