﻿using FinancialManager.Common;
using FinancialManager.DataAccessLayer;
using FinancialManager.DataAccessLayer.Services;
using FinancialManager.Model;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FinancialManager.ViewModel
{
    public class ExpenseEditViewModel : GalaSoft.MvvmLight.ViewModelBase
    {
        private ExpenseService db = null;
        public ICommand SaveCmd { get; private set; }
        public Expense ExpenseItem { get; set; }

        public ExpenseEditViewModel()
        {
            this.SaveCmd = new RelayCommand(Save);
            db = new ExpenseService();
            ExpenseItem = new Expense();

            //DataLoad();
        }

        private async void Save()
        {
            try
            {
                if (String.IsNullOrEmpty(ExpenseItem.Name))
                {
                    //TODO: Message
                    return;
                }

                await db.AddAsync(ExpenseItem);
                //TODO: Msg: Dane zostały zapisane
                NavigateService.Instance.Navigate(typeof(MasterPage));
            }
            catch(Exception ex)
            {
                //TODO: Msg: Wystąpił błąd
            }
        }

        private async Task DataLoad()
        {
            var items = await db.GetListAsync();
        }
    }
}
