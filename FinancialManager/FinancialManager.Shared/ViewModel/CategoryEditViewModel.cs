﻿using FinancialManager.Common;
using FinancialManager.DataAccessLayer.Services;
using FinancialManager.Model;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace FinancialManager.ViewModel
{
    public class CategoryEditViewModel
    {
        public ICommand SaveCmd { get; private set; }
        public Category CategoryItem { get; set; }
        private CategoryService db = null;

        public CategoryEditViewModel()
        {
            this.SaveCmd = new RelayCommand(Save);
            db = new CategoryService();
            CategoryItem = new Category();
        }

        private async void Save()
        {
            try
            {
                if (String.IsNullOrEmpty(CategoryItem.Name))
                {
                    //TODO: Message
                    return;
                }

                await db.AddAsync(CategoryItem);
                //TODO: Msg: Dane zostały zapisane
                NavigateService.Instance.Navigate(typeof(MasterPage));
            }
            catch (Exception ex)
            {
                //TODO: Msg: Wystąpił błąd
            }
        }
    }
}
