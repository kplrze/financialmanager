﻿using FinancialManager.DataAccessLayer.Services;
using FinancialManager.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace FinancialManager.ViewModel
{
    public class CategoriesMainViewModel
    {
        private CategoryService db = null;

        private ObservableCollection<Category> entries = null;
        public ObservableCollection<Category> Entries
        {
            get { return entries; }
            private set { entries = value; }
        }

        public CategoriesMainViewModel()
        {
            db = new CategoryService();

            entries = new ObservableCollection<Category>();

            CategoriesMainViewModelAsync();
        }

        public async Task CategoriesMainViewModelAsync()
        {
            await Init();
            await DataLoad();
        }

        private async Task Init()
        {

        }

        private async Task DataLoad()
        {
            List<Category> items = await db.GetListAsync();
            
            foreach (Category item in items)
            {
                entries.Add(item);
            }
        }
    }
}
