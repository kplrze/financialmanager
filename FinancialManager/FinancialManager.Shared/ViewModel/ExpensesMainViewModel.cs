﻿using FinancialManager.DataAccessLayer;
using FinancialManager.DataAccessLayer.Services;
using FinancialManager.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace FinancialManager.ViewModel
{
    public class ExpensesMainViewModel
    {
        private ExpenseService db = null;

        private ObservableCollection<Expense> entries = null;
        public ObservableCollection<Expense> Entries
        {
            get { return entries; }
            private set { entries = value; }
        }

        public ExpensesMainViewModel()
        {
            db = new ExpenseService();

            entries = new ObservableCollection<Expense>();

            ExpensesMainViewModelAsync();
        }

        private async Task ExpensesMainViewModelAsync()
        {
            await Init();
            await DataLoad();
        }

        private async Task Init()
        {

        }

        private async Task DataLoad()
        {
            List<Expense> items = await db.GetListAsync();
            foreach (Expense item in items)
            {
                entries.Add(item);
            }
        }
    }
}
