﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace FinancialManager.DataAccessLayer.Services
{
    class StorageService
    {
        public async Task SaveTextAsync(string fileName, string text)
        {
            StorageFile file = await ApplicationData.Current.LocalFolder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);

            byte[] data = Encoding.UTF8.GetBytes(text);

            using (var s = await file.OpenStreamForWriteAsync())
            {
                await s.WriteAsync(data, 0, data.Length);
            }
        }

        public async Task<string> LoadTextAsync(string fileName)
        {
            try
            {
                StorageFile file = await ApplicationData.Current.LocalFolder.GetFileAsync(fileName);

                using (var s = await file.OpenStreamForReadAsync())
                {
                    using (var sr = new StreamReader(s))
                    {
                        return await sr.ReadToEndAsync();
                    }
                }
            }
            catch (Exception)
            {
                Debug.WriteLine("Error loading file '{0}'", fileName);
                return null;
            }
        }

        public async Task<string> GetPathForFileAsync(string file)
        {
            StorageFile storageFile = await ApplicationData.Current.LocalFolder.CreateFileAsync(file, CreationCollisionOption.OpenIfExists);

            return storageFile.Path;
        }
    }
}
