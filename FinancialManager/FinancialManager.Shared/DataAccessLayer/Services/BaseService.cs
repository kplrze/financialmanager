﻿using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace FinancialManager.DataAccessLayer.Services
{
    public class BaseService
    {
        protected SQLiteAsyncConnection connection;
        
        protected async Task ConnectAsync()
        {
            if (connection == null)
            {
                //string databaseFile = Path.Combine(ApplicationData.Current.LocalFolder.Path, "Storage.sqlite");
                StorageFile storageFile = await ApplicationData.Current.LocalFolder.CreateFileAsync("Storage.db", CreationCollisionOption.OpenIfExists);
                string databaseFile = storageFile.Path;
                connection = new SQLiteAsyncConnection(databaseFile);

                //test
                await connection.CreateTableAsync<Model.Expense>();
                await connection.CreateTableAsync<Model.Category>();
            }
        }
    }
}
