﻿using FinancialManager.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinancialManager.DataAccessLayer.Services
{
    public class CategoryService : BaseService
    {
        public async Task AddAsync(Category item)
        {
            try
            {
                await ConnectAsync();

                await connection.InsertAsync(item);
            }
            catch (Exception ex)
            {
                //TODO: SaveError
                throw;
            }
        }

        public async Task<List<Category>> GetListAsync()
        {
            bool error = false;
            string errorMsg = String.Empty;
            try
            {
                await ConnectAsync();

                return await connection.Table<Category>().ToListAsync();
            }
            catch (Exception ex)
            {
                error = true;
                errorMsg = ex.Message;
                //TODO: Save error

                //return null;
            }

            if (error)
            {
                StorageService service = new StorageService();
                await service.SaveTextAsync("ErrorLog.txt", errorMsg);
            }
            return null;
        }
    }
}
