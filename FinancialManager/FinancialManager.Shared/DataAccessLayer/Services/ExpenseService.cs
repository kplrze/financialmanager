﻿using FinancialManager.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinancialManager.DataAccessLayer.Services
{
    public class ExpenseService : BaseService
    {
        public async Task AddAsync(Expense item)
        {
            try
            {
                await ConnectAsync();

                await connection.InsertAsync(item);
            }
            catch(Exception ex)
            {
                //TODO: SaveError
                throw;
            }
        }

        public async Task<List<Expense>> GetListAsync()
        {
            bool error = false;
            string errorMsg = String.Empty;
            try
            {
                await ConnectAsync();

                return await connection.Table<Expense>().ToListAsync();
            }
            catch(Exception ex)
            {
                error = true;
                errorMsg = ex.Message;
                //TODO: Save error
               
                //return null;
            }

            if(error)
            {
                StorageService service = new StorageService();
                await service.SaveTextAsync("ErrorLog.txt", errorMsg);
            }
            return null;
        }
    }
}
